#!/usr/bin/env python3
#
#   dependencies:
#       mamba create -n phylo -c bioconda mafft raxml
#
#   usage:
#       make_phylo_from_seqs.py -fasta <filename> -o <outfilesname> [-bootreps <int> -p <int>]
#
#   description:
#       From a set of protein sequences in a FASTA file perform multiple
#       sequence alignment using MAFFT and maximum likelihood-based phylogenetic
#       inference using RAxML.
#
#       The model of protein sequence evolution used for phylogenetic inference
#       is automatically chosen by RAxML.
#
#       MAFFT uses L-INSi algorithm, stated recommended for less than 200
#       sequences; if an alignment of a greater number of sequences is required
#       contact Matt.


import sys
import subprocess as sp
from shutil import copyfile


def showHelp():
    print(
"""
    installation:
    ------------
        mamba create -n phylo -c bioconda mafft raxml
 
    usage:
    ------------
        make_phylo_from_seqs.py -fasta <filename> -o <outfilesname> [-bootreps <int> -p <int>]
 
    description:
    ------------
        From a set of protein sequences in a FASTA file perform multiple
        sequence alignment using MAFFT and maximum likelihood-based phylogenetic
        inference using RAxML.
 
        The model of protein sequence evolution used for phylogenetic inference
        is automatically chosen by RAxML.
 
        MAFFT uses L-INSi algorithm, stated recommended for less than 200
        sequences; if an alignment of a greater number of sequences is required
        contact Matt.
 
    options:
    ------------
    -fasta    <filepath>       A FASTA file with protein sequences.
                               (Required; no default)
    -o        <str>            Basename for output files.
                               (Required; no default)
    -bootreps <int>            The number of bootstrap replicates to perform.
                               (Default: 1000)
    -p        <int>            The number of parallel processes to use for RAxML.
                               (Default: 1)
    -outgroup <str>            Sequence to set as the outgroup. Using this 
                               option will not affect the inference. The RAxML
                               manual states this option should be used with
                               caution because the inclusion of an outgroup can
                               affect branch lengths which may not be desirable.
"""
    )

def getArg(arg, default = None):
    '''Returns the item one index further than the item matching arg in the 
       list sys.argv. If it doesn't exists return default'''

    try:
        argRes = sys.argv[sys.argv.index(arg) + 1]
    except ValueError:
        if default == None:
            exit(f"No default for: {arg}")

        argRes = default

    return (argRes)


# Shortcut to command line arguments
args = sys.argv

# Parse command line arguments
if not ("-fasta" in args and "-o" in args) or "-h" in args:
    showHelp()

fasta = getArg("-fasta")
outname = getArg("-o")
bootreps = getArg("-bootreps", "100")
procs = getArg("-p", "1")
outgroup = getArg("-outgroup", False)

# Align protein sequences
alnfile = f"{outname}.aln.fa"
with open(alnfile, "w") as outf:
    sp.call(["mafft", "--localpair", "--maxiterate", "1000", fasta], stdout = outf)

# Regular bootstrapping
# -b <seed>
# Rapid bootstrapping (<6% difference): use with -f a
# -x <seed>
# Parsimony trees are used as part of the ML algorithms. Random seed for parsimony inferential process:
# -p <int>
# Substitution model
# AUTO = model testing, select best performing
raxCall = ["raxmlHPC-PTHREADS",
           "-T", procs,
           "-m", "PROTGAMMAAUTO",
           "-n", outname,
           "-s", alnfile,
           "-N", bootreps,
           "-p", "123",
           "-f", "a",
           "-x", "123"]

# Perform post-hoc rooting
if outgroup:
    raxCall += ["-o", outgroup]

# Infer phylogeny from protein sequences
sp.call(raxCall)

# Copy final tree with bootstrap values as new name
copyfile(f"RAxML_bipartitions.{outname}", f"{outname}.RAxML.{bootreps}bootreps.newick")
