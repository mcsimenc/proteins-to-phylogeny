### Proteins to Tree: a script to perform protein alignment and phylogenetic inference using maximum likelihood.

```
    installation:
    ------------
        mamba create -n phylo -c bioconda mafft raxml
 
    usage:
    ------------
        make_phylo_from_seqs.py -fasta <filename> -o <outfilesname> [-bootreps <int> -p <int>]
 
    description:
    ------------
        From a set of protein sequences in a FASTA file perform multiple
        sequence alignment using MAFFT and maximum likelihood-based phylogenetic
        inference using RAxML.
 
        The model of protein sequence evolution used for phylogenetic inference
        is automatically chosen by RAxML.
 
        MAFFT uses L-INSi algorithm, stated recommended for less than 200
        sequences; if an alignment of a greater number of sequences is required
        contact Matt.
 
    options:
    ------------
    -fasta    <filepath>       A FASTA file with protein sequences.
                               (Required; no default)
    -o        <str>            Basename for output files.
                               (Required; no default)
    -bootreps <int>            The number of bootstrap replicates to perform.
                               (Default: 1000)
    -p        <int>            The number of parallel processes to use for RAxML.
                               (Default: 1)
    -outgroup <str>            Sequence to set as the outgroup. Using this 
                               option will not affect the inference. The RAxML
                               manual states this option should be used with
                               caution because the inclusion of an outgroup can
                               affect branch lengths which may not be desirable.
```
